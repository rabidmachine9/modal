function Popup(key){
	this.apiKey = key;
    this.jsonData = {};
}


Popup.prototype.onLoad = function(){
	this._callAjax('response-file.php', function(response){
        this.jsonData = JSON.parse(response);

      

        document.body.innerHTML += '<iframe style="border:none;position:absolute;top:0;bottom:0;left:0;right:0;margin:auto;" id="pop_frame" src="" width="300" height="200"></iframe>';
        var iframe = document.getElementById('pop_frame');
        iframedoc = iframe.contentDocument || iframe.contentWindow.document;
        iframedoc.body.innerHTML += '<div id="modal" style="text-align:center;background-color:#'+this.jsonData.color+'"> \
                                        <div class="modalconent" > \
                                            <h1>' + this.jsonData.title +'</h1> \
                                            <p>'+ this.jsonData.text+ '</p> \
                                            <form> \
                                                <textarea>Write something here</textarea><br> \
                                                <input type="submit" value="Submit" id="modalSubmit"> \
                                            </form> \
                                        </div> \
                                    </div>';

        iframedoc.getElementById('modalSubmit').onclick = function () {
            document.getElementById('pop_frame').style.display = "none"
        }
    });
}


Popup.prototype._callAjax = function(url, callback){
    var xmlhttp;

    xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function(){
        if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
            console.log(xmlhttp.responseText)
            callback(xmlhttp.responseText);
        }
    }
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}
